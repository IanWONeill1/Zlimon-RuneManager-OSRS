<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the different titles used in all pages.
    |
    */

    /*==========Pages Titles=============*/
    'index' => 'Home',
    'update-log' => 'Update Log',
    'hiscore' => 'Hiscores',
    'home' => 'Profile',

    /*==========Member Titles=============*/
    'member' => 'Members',
    'search-member' => 'Search results for ',
    'create-member' => 'Link Old School RuneScape account',

    /*==========User Titles=============*/
    'edit-member' => 'Edit profile',

    /*==========Task Titles=============*/
    'task' => 'Tasks',

    /*==========News Titles=============*/
    'news' => 'News posts',

    /*==========Admin Titles=============*/
        /*==========News Titles=============*/
        'create-newspost' => 'Create news post',

    /*==========Auth Titles=============*/
    'login' => 'Login',
    'register' => 'Register',
];
